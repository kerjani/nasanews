package com.kernacs.nasanews.di.component;

import com.kernacs.nasanews.MainActivity;
import com.kernacs.nasanews.MainApplication;
import com.kernacs.nasanews.MainFragment;
import com.kernacs.nasanews.adapter.RssRecyclerViewAdapter;
import com.kernacs.nasanews.adapter.helper.impl.DefaultRssAdapterHelper;
import com.kernacs.nasanews.di.module.NewsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = NewsModule.class)
public interface NewsComponent {
	void inject(RssRecyclerViewAdapter adapter);

	void inject(DefaultRssAdapterHelper rssAdapterHelper);

	void inject(MainApplication application);

	void inject(MainFragment fragment);

	void inject(MainActivity activity);

	class NewsModuleInjector {
		public static final NewsComponent INSTANCE = DaggerNewsComponent.builder()
				.newsModule(NewsModule.INSTANCE)
				.build();
	}
}
