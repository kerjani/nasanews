package com.kernacs.nasanews;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * The main application.
 */
public class MainApplication extends MultiDexApplication {
	public static MainApplication INSTANCE;

	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		INSTANCE = this;
	}

}
