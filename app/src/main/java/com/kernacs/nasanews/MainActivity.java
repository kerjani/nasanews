package com.kernacs.nasanews;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import com.kernacs.nasanews.databinding.ActivityMainBinding;
import com.kernacs.nasanews.di.component.NewsComponent;

/**
 * Main Activity which holds the app logic above the refreshable list
 */
public class MainActivity extends AppCompatActivity {
	private static final String STATE_KEY_QUERY = "MainActivity:Query";

	private String query;

	private ActivityMainBinding activityMainBinding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		NewsComponent.NewsModuleInjector.INSTANCE.inject(this);

		activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
		setSupportActionBar(activityMainBinding.toolbar.toolbar);

		activityMainBinding.fab.setOnClickListener(view -> toggleSearchView());

		// Search text changed listener
		activityMainBinding.toolbar.searchView.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				final Fragment mainFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_main);
				if (mainFragment != null && mainFragment instanceof MainFragment) {
					query = s.toString();
					((MainFragment) mainFragment).search(query);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		activityMainBinding.toolbar.searchView.setOnEditorActionListener((textView, actionId, keyEvent) -> {
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				hideKeyboard();
				return true;
			}
			return false;
		});
		activityMainBinding.toolbar.searchClear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				toggleSearchView();
			}
		});
	}

	private void toggleSearchView() {
		if (activityMainBinding.toolbar.searchContainer.getVisibility() == View.VISIBLE) {
			hideKeyboard();
			activityMainBinding.toolbar.searchView.setText("");
			activityMainBinding.toolbar.searchView.clearFocus();
			activityMainBinding.toolbar.searchContainer.setVisibility(View.GONE);
			activityMainBinding.fab.show();
		} else {
			activityMainBinding.mainAppBarLayout.setExpanded(true, true);
			activityMainBinding.toolbar.searchContainer.setVisibility(View.VISIBLE);
			activityMainBinding.toolbar.searchView.requestFocus();
			// Pop up the soft keyboard
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					activityMainBinding.toolbar.searchView.dispatchTouchEvent(MotionEvent.obtain(
							SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
					activityMainBinding.toolbar.searchView.dispatchTouchEvent(MotionEvent.obtain(
							SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
				}
			}, 200);
			activityMainBinding.fab.hide();
		}
	}

	private void hideKeyboard() {
		final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(activityMainBinding.toolbar.searchView.getWindowToken(), 0);
	}

	@Override
	public void onBackPressed() {
		if (activityMainBinding.toolbar.searchContainer.getVisibility() == View.VISIBLE) {
			toggleSearchView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		if (!TextUtils.isEmpty(query)) {
			outState.putString(STATE_KEY_QUERY, query);
		}
	}

	@Override
	protected void onRestoreInstanceState(final Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null && savedInstanceState.containsKey(STATE_KEY_QUERY)) {
			query = savedInstanceState.getString(STATE_KEY_QUERY);
			activityMainBinding.toolbar.searchContainer.setVisibility(View.VISIBLE);
			activityMainBinding.toolbar.searchView.setText(query);
			activityMainBinding.toolbar.searchView.requestFocus();
			activityMainBinding.fab.hide();
		}
	}

}
