package com.kernacs.nasanews.adapter;

import android.support.v7.util.DiffUtil;

import com.kernacs.nasanews.communication.data.FeedItem;

import java.util.List;

/**
 * A Callback class used by DiffUtil while calculating the diff between the old and new lists.
 */
public class FeedItemDiffCallback extends DiffUtil.Callback {

	private List<FeedItem> oldList;

	private List<FeedItem> newList;

	public FeedItemDiffCallback(final List<FeedItem> oldList, final List<FeedItem> newList) {
		this.oldList = oldList;
		this.newList = newList;
	}

	@Override
	public int getOldListSize() {
		return oldList.size();
	}

	@Override
	public int getNewListSize() {
		return newList.size();
	}

	@Override
	public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
		// add a unique ID property on FeedItem and expose a getId() method
		return oldList.get(oldItemPosition).getLink().equals(newList.get(newItemPosition).getLink());
	}

	@Override
	public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
		final FeedItem oldFeedItem = oldList.get(oldItemPosition);
		final FeedItem newFeedItem = newList.get(newItemPosition);

		if (oldFeedItem.getLink().equals(newFeedItem.getLink())) {
			return true;
		}
		return false;
	}

}
