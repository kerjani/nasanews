package com.kernacs.nasanews.adapter.helper;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

/**
 * Helper interface for use in view holders.
 */
public interface RssAdapterHelper {

	/**
	 * Opens an URL in a browser.
	 *
	 * @param url the URL to open.
	 */
	void openUrl(@NonNull final View view, @NonNull final String url);

	/**
	 * Loads the image from the specified URL into the image view.
	 *
	 * @param imageView the image view to load the image into.
	 * @param imageUrl  the URL to load the image from.
	 */
	@BindingAdapter("bind:imageUrl")
	void loadImage(final ImageView imageView, final String imageUrl);
}
