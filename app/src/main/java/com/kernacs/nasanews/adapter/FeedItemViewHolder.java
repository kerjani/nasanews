package com.kernacs.nasanews.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.kernacs.nasanews.databinding.ItemRssBinding;

/**
 * View holder for {@link android.support.v7.widget.RecyclerView.Adapter}.
 */
public class FeedItemViewHolder extends RecyclerView.ViewHolder {

	protected final ItemRssBinding binding;

	/**
	 * Constructor for initialization by a view binding.
	 *
	 * @param binding view binding.
	 */
	public FeedItemViewHolder(final ViewDataBinding binding) {
		super(binding.getRoot());
		this.binding = DataBindingUtil.bind(binding.getRoot());
	}

	/**
	 * Gets the item binding.
	 *
	 * @return the item binding
	 */
	public ItemRssBinding getBinding() {
		return binding;
	}
}
