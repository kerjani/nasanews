package com.kernacs.nasanews.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kernacs.nasanews.BR;
import com.kernacs.nasanews.R;
import com.kernacs.nasanews.adapter.helper.RssAdapterHelper;
import com.kernacs.nasanews.communication.data.FeedItem;
import com.kernacs.nasanews.di.component.NewsComponent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Adapter for the recycler view of the RSS items.
 */
public class RssRecyclerViewAdapter extends RecyclerView.Adapter<FeedItemViewHolder> {

	@Inject
	protected RssAdapterHelper rssAdapterHelper;

	private List<FeedItem> items;

	private List<FeedItem> visibleItems;

	private String queryText;

	/**
	 * Constructor for initializing and injecting fields.
	 */
	@Inject
	public RssRecyclerViewAdapter() {
		items = Collections.EMPTY_LIST;
		visibleItems = Collections.EMPTY_LIST;
		NewsComponent.NewsModuleInjector.INSTANCE.inject(this);
	}

	@Override
	public FeedItemViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		final ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
				R.layout.item_rss, parent, false,
				new android.databinding.DataBindingComponent() {
					@Override
					public RssAdapterHelper getRssAdapterHelper() {
						return rssAdapterHelper;
					}
				});
		return new FeedItemViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(final FeedItemViewHolder holder, final int position) {
		final FeedItem item = visibleItems.get(position);
		holder.getBinding().setVariable(BR.item, item);
		holder.getBinding().setVariable(BR.helper, rssAdapterHelper);
		holder.getBinding().executePendingBindings();
	}

	@Override
	public int getItemCount() {
		return visibleItems.size();
	}

	public void swapItems(List<FeedItem> feedItems) {
		// compute diffs
		final FeedItemDiffCallback diffCallback = new FeedItemDiffCallback(this.visibleItems, feedItems);
		final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

		this.visibleItems = new ArrayList<>(feedItems);

		diffResult.dispatchUpdatesTo(this);
	}

	/**
	 * Sets the items and filters with a quest if it exists.
	 *
	 * @param items The items to set
	 */
	public void setItems(final List<FeedItem> items) {
		this.items = items;
		flushItems();
	}

	public List<FeedItem> getItems() {
		return items;
	}

	private void flushItems() {
		final ArrayList<FeedItem> newItems = new ArrayList<>();
		if (TextUtils.isEmpty(queryText)) {
			newItems.addAll(items);
		} else {
			final String loverCaseQuery = queryText.toLowerCase();
			for (FeedItem item : items) {
				if (item.getTitle().toLowerCase().contains(loverCaseQuery)) {
					newItems.add(item);
				}
			}
		}
		swapItems(newItems);
	}

	/**
	 * Sets the search query and filters the items of the adapter.
	 *
	 * @param queryText the search query.
	 */
	public void setFilter(String queryText) {
		this.queryText = queryText;
		flushItems();
	}
}
