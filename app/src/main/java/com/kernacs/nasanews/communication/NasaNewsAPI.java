package com.kernacs.nasanews.communication;

import com.kernacs.nasanews.communication.data.Feed;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Collection of the API endpoints.
 */
public interface NasaNewsAPI {

	/**
	 * API endopint for getting the RSS feed.
	 *
	 * @return {@link Observable} instance which provides the {@link Feed}.
	 */
	@GET("./")
	Observable<Feed> getItems();
}
