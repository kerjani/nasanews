package com.kernacs.nasanews.communication.data;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Main root item of the feed.
 */
@Root(name = "rss", strict = false)
public class Feed implements Serializable {

	@Element(name = "channel")
	private Channel channel;

	public Feed() {
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(final Channel channel) {
		this.channel = channel;
	}
}
