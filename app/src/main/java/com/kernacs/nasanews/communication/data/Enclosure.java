package com.kernacs.nasanews.communication.data;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Class for the tag which conteins the image URL.
 */
@Root(name = "enclosure", strict = false)
public class Enclosure implements Serializable {

	@Attribute
	private String url;

	@Attribute
	private String length;

	@Attribute
	private String type;

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getLength() {
		return length;
	}

	public void setLength(final String length) {
		this.length = length;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}
}
