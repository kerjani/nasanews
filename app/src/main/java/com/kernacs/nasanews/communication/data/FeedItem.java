package com.kernacs.nasanews.communication.data;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Item of the RSS feed list.
 */
@Root(name = "item", strict = false)
public class FeedItem implements Serializable {

	@Element(name = "pubDate")
	private String pubDate;

	@Element(name = "title")
	private String title;

	@Element(name = "link")
	private String link;

	@Element(name = "description")
	private String description;

	@Element(name = "enclosure")
	private Enclosure enclosure;

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Enclosure getEnclosure() {
		return enclosure;
	}

	public void setEnclosure(final Enclosure enclosure) {
		this.enclosure = enclosure;
	}
}
